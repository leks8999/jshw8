/* Ответы на теоретические вопросы:
1. DOM – это объектная модель документа, которую браузер создаёт в памяти компьютера
на основании HTML - кода.
2. innerHTML выведет текст вместе с разметкой html(теги),
    а innerText покажет и текст и разметку как обычный текст.
3. К елементу страницы можно обратиться с помощью document.querySelectorAll
или getElements.document.querySelectorAll лучше, с помощью него можно обратиться К
любому элементу.Если к html - document.documentElement, document.body, document.head. */


/* const paragraphs = document.querySelectorAll('p')
for (const item of paragraphs) {
    item.style.backgroundColor = '#ff0000';
    console.log(item);
}
 */
/* const elem = document.getElementById('optionsList')
console.log(elem);
const parent = elem.parentElement
console.log(parent);
const childNodes = elem.childNodes
for (const item of childNodes) {
    console.log(item);
} */

const main = document.querySelector('.main-logo')
/* const newElem = main.insertAdjacentHTML('afterbegin', `<p class='testParagraph'>This is a paragraph</p>`) */
const newElem = document.createElement('p')
newElem.className = 'testParagraph' //newElem.classList.add('testParagraph')
newElem.innerHTML = 'This is a paragraph'
main.after(newElem)
console.log(newElem);

/* const el = document.querySelector('.main-header')
const el2 = el.children
for (const item of el2) {
    console.log(item);
    item.classList.add('nav-item')
}*/

/* const el = document.querySelectorAll('.section-title')
for (const item of el) {
    item.classList.remove('section-title')
    console.log(item);
} */





